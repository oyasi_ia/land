# -*- coding: utf-8 -*-
from django.conf.urls import url

from .views import preview_landing, edit_landing, moderating


urlpatterns = [
    url(r'^(?P<pk>\d+)/view$', preview_landing, name="preview_landing"),
    url(r'^(?P<pk>\d+)/edit$', edit_landing, name="edit_landing"),
    url(r'^(?P<pk>\d+)/moderating$', moderating, name="moderating"),
]
