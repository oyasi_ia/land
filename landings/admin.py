# coding: utf-8
from django.contrib import admin
from django.core.exceptions import ObjectDoesNotExist
from .models import Landing, LandingImage, ElementTimeline, LandingArchive
import json
from deepdiff import DeepDiff
from shared.admin import BaseAdmin

class LandingAdmin(BaseAdmin):
    change_form_template = 'admin/landings/landing/change_form.html'

    fields = ('title', 'moderating', 'publish', 'creater', 'domain',)
    raw_id_fields = ('creater', )
    list_filter = ('moderating', 'domain',)
    list_display = ('title', 'moderating', 'publish',)

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return self.readonly_fields + ('moderating', 'publish')
        return self.readonly_fields

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['landing'] = []

        def parst_dict(item, parsabeKeys=('elements', 'quote', 'point'), unparsableKey=('template', 'order', 'black_list')):
            arr = []
            for key, value in item.items():
                if key not in parsabeKeys:
                    if key not in unparsableKey:
                        arr.append({key: value})
                else:
                    for el in value:
                        arr += parst_dict(el)
            arr.reverse()
            return arr

        try:
            obj = Landing.objects.get(id=object_id)
        except Landing.DoesNotExist:
            pass

        old_obj = LandingArchive.objects.filter(landing=obj).last()
        if old_obj is not None:
            ddiff = DeepDiff(old_obj.data, obj.data)
            for item in ddiff.get('iterable_item_removed', []):
                extra_context['landing'].append({'removed': parst_dict(ddiff['iterable_item_removed'][item])})
            for item in ddiff.get('iterable_item_added', []):
                extra_context['landing'].append({'added': parst_dict(ddiff['iterable_item_added'][item])})
            for item in ddiff.get('values_changed', []):
                if 'src' in item:
                    extra_context['landing'].append({'changed': [{'img': {'src': ddiff['values_changed'][item]['new_value']}}]})
                else:
                    extra_context['landing'].append({'changed': [{'text': ddiff['values_changed'][item]['new_value']}]})
        else:
            for item in obj.data:
                extra_context['landing'].append({'added': parst_dict(item)})

        extra_context['moderating'] = obj.moderating

        return super(LandingAdmin, self).change_view(
            request, object_id, form_url, extra_context=extra_context,
        )


admin.site.register(Landing, LandingAdmin)
admin.site.register(LandingImage, BaseAdmin)
admin.site.register(ElementTimeline, BaseAdmin)
