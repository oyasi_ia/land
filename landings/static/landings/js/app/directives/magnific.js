angular.module('landApp').directive('magnificImage', function(){
    return {
        restrict: 'A',
        link: function (scope, iElement) {
            scope.$evalAsync(function () {
                iElement.magnificPopup({
                    delegate: 'a',
                    type: 'image',
                    mainClass: 'mfp-with-zoom',
                    zoom: {
                        enabled: true,
                        duration: 300,
                        easing: 'ease-in-out'
                    },
                    gallery: {
                        enabled: true,
                        arrowMarkup:
                            '<a class="magnific__arrow magnific__arrow_%dir%">' +
                                '<svg class="svg-popup-arrow-prev mfp-prevent-close" xmlns="http://www.w3.org/2000/svg" width="28.125" height="54" viewBox="0 0 28.125 54">' +
                                    '<path class="cls-1 mfp-prevent-close" d="M42.4,213.006L15.994,186.5,42.4,159.994" transform="translate(-15.5 -159.5)"/>' +
                                '</svg>' +
                                '<svg class="svg-popup-arrow-next mfp-prevent-close" xmlns="http://www.w3.org/2000/svg" width="28" height="54" viewBox="0 0 28 54">' +
                                    '<path class="cls-1 mfp-prevent-close" d="M1807.72,164.994l26.29,26.506-26.29,26.506" transform="translate(-1806.5 -164.5)"/>' +
                                '</svg>' +
                            '</a>'
                    },
                    image: {
                        cursor: null,
                        markup: '<div class="mfp-figure">'+
                            '<div class="mfp-img"></div>'+
                            '<div class="mfp-bottom-bar">'+
                              '<div class="mfp-title"></div>'+
                              '<div class="mfp-counter"></div>'+
                            '</div>'+
                          '</div>',
                    },
                    closeMarkup: '<a class="magnific__close mfp-close"><span class="icon-close"></span></a>',
                    closeBtnInside: false
                });
            });
        }
    };
});
