angular.module('landApp').config(function ($stateProvider, $locationProvider, $urlRouterProvider) {
    $locationProvider.html5Mode({enabled: true, requireBase: false});
    $urlRouterProvider.otherwise("/")

    $stateProvider
        .state("landings", {
            abstract: true,
            controller: "MainController",
            template: "<div ui-view></div>"
        })
        .state("landings.edit", {
            url: "/:id/edit",
            controller: "LandingEditorController",
            templateUrl: "/static/landings/views/edit.html",
            resolve: {
                landing: function(Landing, $stateParams) {
                    return Landing.get({"id": $stateParams.id}).$promise
                },
                actions: function(Bonus){
                    return Bonus.get({}).$promise
                }
            }
        })
});
