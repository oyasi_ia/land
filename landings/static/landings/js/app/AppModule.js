import uiRouter from 'angular-ui-router'
import ngSanitize from 'angular-sanitize'
import 'angular-loading-bar'
import ngFileUpload from 'ng-file-upload'
import 'angular-translate'

'use strict';

angular.module('landApp', [
    uiRouter,
    ngSanitize,
    ngFileUpload,
    'slickCarousel',
    'ngResource',
    'angular-loading-bar',
    'ngScrollbars',
    'thatisuday.dropzone',
    'pascalprecht.translate'
]);

angular.module('landApp').config(function ($provide, $httpProvider, $translateProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

    $translateProvider.translations('en', require('./translation.en.js'));
    $translateProvider.translations('ru', require('./translation.ru.js'));

});
