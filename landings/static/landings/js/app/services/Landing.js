angular.module('landApp').factory('Landing', ["$resource",
    function ($resource) {
        return $resource("/api/landing/:id", {id: "@id"},
            {
                put: {method: "PUT"},
                patch: {method: "PATCH"},
            });
    }]);
