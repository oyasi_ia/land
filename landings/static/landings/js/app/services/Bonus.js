angular.module('landApp').factory('Bonus', ["$resource",
    function ($resource) {
        return $resource("/api/bonus/bonus_description/:id", {id: "@id"},
            {
                put: {method: "PUT"},
                patch: {method: "PATCH"},
            });
    }]);
