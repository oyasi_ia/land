module.exports = {
    'Поднять блок': 'Move to up',
    'Опустить блок': 'Move to down',
    'Выбрать фон': 'Choose background',
    'Удалить блок': 'Delete',
    'Добавить блок': 'Add block',
    'Скрыть панель': 'Hide panel',
    'Добавить элемент': 'Add element',
    'Добавить изображение': 'Add image',
    'Имя': 'Name',
    'Телефон': 'Phone',
    'Лучший форекс-брокер в Африке 2017': 'Best Forex Broker in Africa 2017',
    award_africa_17: 'The ceremony took place during the "Africa Financial Expo 2017" in Johannesburg, South Africa.',
    'Лучшие технологии для трейдинга 2016': 'Best trading technology 2016',
    award_tech_16: 'The award ceremony took place during the annual Moscow Financial Expo on the 3rd of November 2016 in Moscow, Russia.',
    'Лучшая партнерская программа 2016': 'Best Affiliate Program 2016',
    award_affiliate_16: 'The award ceremony took place during the 16th MENA Financial Expo on 8th of April 2016 in Dubai, UAE.',
    'Диплом': 'Diploma',
    'Лучший брокер в Европе 2015': 'Best broker in Europe 2015',
    award_europe_15: 'In 2015 Grand Capital was recognized as the best broker in Europe by ForexStars - independent Forex tournament project.',
    'Стабильность на рынке Forex': 'Stability on the Forex market',
    award_stability: 'The best broker cup Grand Capital was received from MasterForex-V Academy after the voting by traders and Academy administration.',
    'Кубок': 'Cup',
    'Лучший брокер бинарных опционов': 'Forex Expo Awards Best Binary Option Broker',
    'Лучший Форекс Брокер в Китае': 'Best Forex Broker in China',
    award_china_14: 'As a participant of CHINA SHANGHAI FOREX EXPO 2014, China, Shanghai, 9.12.2014',
    'Добавить отзыв': 'Add review',
    'Добавить пункт обучения': 'Add point',
    'Добавить цитату': 'Add quote',
    'Выбрать видео': 'Choose video',
    'Опубликовать': 'Publish',
    'Предпросмотр': 'Preview',
    'Отмена': 'Cancel',
    'Вернуться на сайт': 'Back to site',
    'Уведомление о рисках': 'Risk disclosure',
    risk_disclosure: 'before starting to trade on currency exchange markets, please make sure that you understand the risks connected with trading using leverage and that you have sufficient level of training.',
    'Политика конфиденциальности': 'Privacy policy',
    privacy_policy: ' describes in which way Company collects, keeps and protects Clients personal data',
    'Закрыть': 'Close'
}
