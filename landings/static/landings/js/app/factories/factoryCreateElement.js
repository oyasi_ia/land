angular.module('landApp').factory('createElement', function() {
    function advantages_img_elements () {
        return {
            'template': 'advantages_img_elements',
            'title': 'Эффективность',
            'text': 'Даже всемогущая пунктуация не имеет власти над рыбными текстами, ведущими безорфографичный образ жизни.',
            'img': {'src': '/static/landings/images/advantages/advantages1.png'}
        }
    }

    function advantages_number_elements () {
        return {
            'template': 'advantages_number_elements',
            'title': 'Эффективность',
            'text': 'Даже всемогущая пунктуация не имеет власти над рыбными текстами, ведущими безорфографичный образ жизни.',
        }
    }

    function gallery () {
        return {
            'img': {'src': 'http://lorempixel.com/output/cats-q-c-640-480-1.jpg'}
        }
    }

    function reviews_elements (key) {
        return {
            'title': 'Сергей Ванеев',
            'text': '«В процессе обучения мы выясняем потребности будущих инвесторов и вместе решаем, на какую стратегию сделать упор. Если человек пришел учиться инвестировать, то мы концентрируемся на стратегии сбалансированных рисков. Если человек интересуется быстрыми заработками – мы изучаем стратегии валютного рынка.»',
            'template': 'reviews'+key+'_elements',
            'img': {'src': '/static/landings/images/stas.jpg'}
        }
    }

    function learning_quote_elements () {
        return {
            'template': 'learning_quote_elements',
            'img': {'src': '/static/landings/images/stas.jpg'},
            'title': 'Сергей Ванеев',
            'text': '«В процессе обучения мы выясняем потребности будущих инвесторов и вместе решаем, на какую стратегию сделать упор. Если человек пришел учиться инвестировать, то мы концентрируемся на стратегии сбалансированных рисков. Если человек интересуетсaadя быстрыми заработками – мы изучаем стратегии валютного рынка.»',
            'comment': 'Аналитик'
        }
    }

    function learning_point_elements () {
        return {
            'template': 'learning_point_elements',
            'img': {'src': '/static/landings/images/stas.jpg'},
            'title': 'Вводная лекция',
            'text': 'структура мирового финансового рынка профессия — трейдер способы увеличения капитала с помощью услуг компании Grand Capital для частных трейдеров',
        }
    }

    function learning2_quote_elements () {
        return {
            'template': 'learning2_quote_elements',
            'img': {'src': '/static/landings/images/stas.jpg'},
            'title': 'Сергей Ванеев',
            'text': '«В процессе обучения мы выясняем потребности будущих инвесторов и вместе решаем, на какую стратегию сделать упор. Если человек пришел учиться инвестировать, то мы концентрируемся на стратегии сбалансированных рисков. Если человек интересуетсaadя быстрыми заработками – мы изучаем стратегии валютного рынка.»',
            'comment': 'Аналитик'
        }
    }

    function learning3_point_elements () {
        return {
            'template': 'learning3_point_elements',
            'img': {'src': '/static/landings/images/stas.jpg'},
            'title': 'Сергей Ванеев',
            'text': 'структура мирового финансового рынка профессия — трейдер способы увеличения капитала с помощью услуг компании Grand Capital для частных трейдеров',
            'comment': 'Вводная лекция',
        }
    }

    return {
        advantages_number_elements,
        advantages_img_elements,
        gallery,
        reviews_elements,
        learning_quote_elements,
        learning_point_elements,
        learning2_quote_elements,
        learning3_point_elements,
    }

});
