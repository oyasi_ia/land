angular.module('landApp').factory('addBlock', function(createElement, $rootScope) {

    function _block_init(template, order) {
        return {'template': template, 'order': order+1}
    }

    function _addblock_customtitle(template, order) {
        var block = _block_init(template, order)
        block['title'] = 'Я заголовок'
        return block
    }

    function _addblock_customtext(template, order) {
        var block = _block_init(template, order)
        block['text'] = 'Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана. Маленький ручеек Даль журчит по всей стране и обеспечивает ее всеми необходимыми правилами.'
        return block
    }

    function _addblock_customtext_center(template, order) {
        var block = _block_init(template, order)
        block['text'] = 'Я текст по центру. Я собрал семь своих заглавных букв, подпоясал инициал за пояс и пустился в дорогу. Взобравшись на первую вершину курсивных гор, бросил он последний взгляд назад, на силуэт своего родного города Буквоград, на заголовок деревни Алфавит и на подзаголовок своего переулка Строчка.'
        return block
    }

    function _addblock_customtext_columns(template, order) {
        var block = _block_init(template, order)
        block['text_1'] = 'Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана. Маленький ручеек Даль журчит по всей стране и обеспечивает ее всеми необходимыми правилами.'
        block['text_2'] = 'Эта парадигматическая страна, в которой жаренные члены предложения залетают прямо в рот. Даже всемогущая пунктуация не имеет власти над рыбными текстами, ведущими безорфографичный образ жизни. Однажды маленькая строчка по имени Lorem ipsum решила выйти в большой мир грамматики.'
        return block
    }

    function _addblock_custombutton(template, order) {
        var block = _block_init(template, order)
        block['button'] = 'Открыть счет'
        return block
    }

    function _addblock_about(template, order) {
        var block = _block_init(template, order)
        block['title'] = 'О компании'
        return block
    }

    function _addblock_action(template, order) {
        var block = _block_init(template, order)
        block['title'] = 'Акции'
        block['black_list'] = []
        return block
    }

    function _addblock_awards(template, order) {
        var block = _block_init(template, order)
        block['title'] = 'Награды'
        return block
    }

    function _addblock_intro(template, order) {
        var block = _block_init(template, order)
        block['title'] = 'The simpliest way to bring your ideas to lorem'
        block['img'] = {'src': '/static/landings/images/main-screen.jpg'}
        block['text'] = 'liquam varius fringilla imperdiet sodales sapien tincidunt suscipit amet lacus sed cursus porta, egestas vivamus ultrices luctus feugiat rabitur nulla arcu, a sollice tudin felis lacus sed cursus porta imperdiet sodales'
        if(template === 'intro_1'){
            block['button'] = 'Открыть счет'
        }else{
            block['form_title'] = 'Регистрация'
            block['button'] = 'Зарегистрироваться'
        }
        return block
    }

    function _addblock_advantages(template, order) {
        var block = _block_init(template, order)
        block['title'] = 'Преимущества'
        block['elements'] = []
        if(template === 'advantages_1' || template === 'advantages_2'){
            for (var i = 0; i < 3; i++) {
                block['elements'].push(createElement.advantages_img_elements());
            }
        }else{
            for (var i = 0; i < 3; i++) {
                block['elements'].push(createElement.advantages_number_elements());
            }
        }
        return block
    }

    function _addblock_gallery(template, order) {
        var block = _block_init(template, order)
        block['title'] = 'Галерея изображений'
        block['text'] = 'лучший способ заработать прямо сейчас!'
        block['elements'] = []
        for (var i = 0; i < 3; i++) {
            block['elements'].push(createElement.gallery());
        }
        return block
    }

    function _addblock_product(template, order) {
        var block = _block_init(template, order)
        block['img'] = {'src': '/static/landings/images/product.png'}
        block['title'] = 'Описание продукта'
        block['text'] = 'Лучший способ заработать прямо сейчас!'
        block['text2'] = 'Он собрал семь своих заглавных букв, подпоясал инициал за пояс и пустился в дорогу. Взобравшись на первую вершину курсивных гор, бросил он последний взгляд назад, на силуэт своего родного города Буквоград, на заголовок деревни Алфавит и на подзаголовок своего переулка Строчка. Однажды одна маленькая строчка рыбного текста по имени Lorem ipsum решила выйти в большой мир грамматики. Он собрал семь своих заглавных букв, подпоясал инициал за пояс и пустился в дорогу. Взобравшись на первую вершину курсивных гор, бросил он последний взгляд назад, на силуэт своего родного города Буквоград, на заголовок деревни Алфавит и на подзаголовок своего переулка Строчка.'
        return block
    }

    function _addblock_reviews(template, order) {
        var block = _block_init(template, order)
        block['title'] = 'Отзывы клиентов'
        block['elements'] = []
        if(template === 'reviews_1'){
            for (var i = 0; i < 3; i++) {
                block['elements'].push(createElement.reviews_elements(1));
            }
        }else{
            for (var i = 0; i < 3; i++) {
                block['elements'].push(createElement.reviews_elements(2));
            }
        }
        return block
    }

    function _addblock_registration(template, order) {
        var block = _block_init(template, order)
        block['title'] = 'Регистрация'
        block['button'] = 'Открыть счет'
        block['img'] = {'src': '/static/landings/images/registration.jpg'}
        if(template === 'registration_1'){
            block['text'] = 'Начните зарабатывать прямо сейчас!'
        }
        return block
    }

    function _addblock_learning(template, order) {
        var block = _block_init(template, order)
        block['title'] = 'Обучение'
        block['button'] = 'Начать обучение'
        if(template === 'learning_1') {
            block['quote'] = []
            block['point'] = []
            block['quote'].push(createElement.learning_quote_elements());
            for (var i = 0; i < 3; i++) {
                block['point'].push(createElement.learning_point_elements());
            }
        }
        if(template === 'learning_2') {
            block['quote'] = []
            for (var i = 0; i < 3; i++) {
                block['quote'].push(createElement.learning2_quote_elements());
            }
        }
        if(template === 'learning_3') {
            block['point'] = []
            for (var i = 0; i < 3; i++) {
                block['point'].push(createElement.learning3_point_elements());
            }
        }
        return block
    }

    function _addblock_video(template, order) {
        var block = _block_init(template, order)
        block['src'] = 'HFhOZT_8zys'
        block['title'] = 'Видео'
        return block
    }

    return {
        _addblock_customtitle,
        _addblock_customtext,
        _addblock_customtext_center,
        _addblock_customtext_columns,
        _addblock_custombutton,
        _addblock_about,
        _addblock_action,
        _addblock_awards,
        _addblock_intro,
        _addblock_advantages,
        _addblock_gallery,
        _addblock_product,
        _addblock_reviews,
        _addblock_registration,
        _addblock_learning,
        _addblock_video,
    }

});
