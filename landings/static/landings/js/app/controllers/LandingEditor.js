angular.module('landApp').controller('LandingEditorController',
    function ($scope, $rootScope, $timeout, $stateParams, landing, $http, actions, $translate) {
        $rootScope.dataLoaded = $rootScope.actionsLoaded = true;
        $scope.blocks = landing.data;
        $scope.blockCash = [];
        $scope.landing = landing;
        $scope.change = false;
        $scope.actions = actions.results;
        $scope.language = 'en'; // delete me plz

        $translate.use($scope.language);

        $translate(['Перетащите файл сюда', 'Выбрать файл']).then(function(translate){
            $scope.dzmsg = translate['Перетащите файл сюда']
            $scope.dzbtn = translate['Выбрать файл']
        })

        var copyBlocks = function(from_array, to_array){
            $rootScope.dataLoaded = $rootScope.actionsLoaded = $rootScope.galleryLoaded = false;
            from_array.forEach(function(block){
                var obj = {}
                for (var key in block){
                    obj[key] = angular.copy(block[key])
                }
                to_array.push(obj)
                if (from_array.length == to_array.length){
                    $timeout(function() {
                        $rootScope.dataLoaded = $rootScope.actionsLoaded = $rootScope.galleryLoaded = true;
                    })
                }
            })
        }

        $rootScope.changeAndSave = function(value){
            $scope.change = value;
            $scope.landing.data = $scope.blocks;
            $http.patch('/api/landing/'+$scope.landing.id, $scope.landing)
            $scope.blockCash = [];
            copyBlocks($scope.blocks, $scope.blockCash);
        }

        $scope.publish = function(){
            $scope.sending = true;
            $scope.landing.moderating = true;
            $http.patch('/api/landing/'+$scope.landing.id, $scope.landing);
            $scope.openModal('modal-ok');
            $timeout(function() {
                $scope.sending = false;
            }, 600)
        }

        $scope.cancel = function(){
            $scope.blocks = [];
            copyBlocks($scope.blockCash, $scope.blocks);
        }

        copyBlocks($scope.blocks, $scope.blockCash);

        $(document).foundation();

    	$scope.panelState = false;
        $scope.modalState = false;
        $scope.currentViewPanel = undefined;
        $scope.currentModal = undefined;
        $scope.showCarousel = false;

        //general setting dropzone
        $scope.previewTemplate =
            '<div class="dropzone__file">' +
              '<a class="dropzone__remove" data-dz-remove><span class="icon-close"></span></a>' +
              '<div class="dropzone__image"><img data-dz-thumbnail /></div>' +
             '<div class="dropzone__error"><span data-dz-errormessage></span></div>' +
            '</div>';


        //general setting scrollbar
    	$scope.scrollbarConfig = {
		    autoHideScrollbar: false,
		    theme: 'scroll',
		    advanced:{
		        updateOnContentResize: true
		    },
	        scrollInertia: 500
	    };

    	$rootScope.togglePanel = function(order){
    		$scope.panelState = !$scope.panelState;
    		$scope.currentViewPanel = undefined;
            $scope.order = order;
    	};

        $scope.setVideoOrder = function(order){
			$scope.videoOrder = order;
		}

        $scope.setElementForChange = function(block, index){
			$scope.elementForChange = $scope.blocks[$scope.blocks.indexOf(block)].elements[index];
		}

        $scope.deleteElement = function(block, index){
            $rootScope.changeAndSave(true);
            if(block == 'gallery_1' || block == 'gallery_2'){
                var slick_template = $rootScope.galleryLoaded
            }else{
                var slick_template = $rootScope.dataLoaded
            }
            slick_template = false
            $scope.blocks[$scope.blocks.indexOf(block)].elements.splice(index, 1)
            $timeout(function() {
                slick_template = true;
            });
        }

        $scope.deleteQuote = function(block, index){
            $rootScope.changeAndSave(true);
            $scope.blocks[$scope.blocks.indexOf(block)].quote.splice(index, 1)
        }

        $scope.deletePoint = function(block, index){
            $rootScope.changeAndSave(true);
            $scope.blocks[$scope.blocks.indexOf(block)].point.splice(index, 1)
        };

        $scope.changeSrc = function($event){
            $rootScope.changeAndSave(true);
            $scope.blockForChange.img.src = angular.element($event.currentTarget).find('img').attr('src')
        }

        $scope.changeIcon = function($event){
            $rootScope.changeAndSave(true);
            $scope.elementForChange.img.src = angular.element($event.currentTarget).find('img').attr('src')
        }

        $scope.closeViewPanel = function(){
            $scope.currentViewPanel = undefined;
        };

    	$scope.openViewPanel = function(viewPanelName){
    		$scope.currentViewPanel = viewPanelName;
    	};

        $scope.openModal = function(modalName){
            $scope.showCarousel = true;
            $scope.modalState = true;
            $scope.currentModal = modalName;
        };

        $scope.closeModal = function(){
            $scope.showCarousel = false;
            $scope.modalState = false;
            $scope.currentModal = undefined;
        };

    });
