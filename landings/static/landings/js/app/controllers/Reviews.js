angular.module('landApp').controller('ReviewsController',
	function ($scope, $http, $stateParams, Upload, $rootScope) {

		// настройки для Dropzone
        $scope.dzBgOptions = {
            url: '/',
            autoProcessQueue: false,
            acceptedFiles : 'image/jpeg, images/jpg, image/png',
            addRemoveLinks : false,
            uploadMultiple: false,
            thumbnailWidth: 300,
            thumbnailHeight: 300,
            maxFiles: 1,
            dictDefaultMessage:
                '<div class="dropzone__message">'+$scope.dzmsg+'</div>' +
                '<span class="button button_base button_red dropzone__button">'+$scope.dzbtn+'</span>',
            previewTemplate: $scope.previewTemplate
        };

		$scope.dzBgCallbacks = {
            'addedfile': function(file) {
                var allFiles = $scope.dzBgMethods.getAllFiles();
                if (allFiles.length > 1) {
                    $scope.dzBgMethods.removeFile(allFiles[0]);
                }
                $scope.file = file;
            },
            'removedfile': function() {
                $scope.file = null;
            }
        };

        $scope.dzBgMethods = {};

        $scope.setSlugPic = function(order, template, index){
            $rootScope.slug_pic = order +'-'+ template+'-'+index;
        };

        var updateImg = function(data, obj, type){
			$rootScope.changeAndSave();
            Upload.upload({
                url: '/api/landing_image/'+data.data.id,
                data: {"img": $scope.file},
                method: 'PATCH'
            }).then(function(data){
                obj[type][$rootScope.slug_pic.split("-")[2]]['img']['src'] = data.data.img
                obj[type][$rootScope.slug_pic.split("-")[2]]['img']['id'] = data.data.id
                $scope.dzBgMethods.removeFile($scope.file);
            })
        }

        $scope.upload = function() {
            var obj = $scope.blocks.find(d=>d.order == $rootScope.slug_pic.split("-")[0] && d.template == $rootScope.slug_pic.split("-")[1])
			var type = 'elements'
			if('point' in obj){
				var type = 'point'
			}
			if('quote' in obj){
				var type = 'quote'
			}
            if('id' in obj[type][$rootScope.slug_pic.split("-")[2]]['img']){
                $http.patch('/api/landing_image/'+obj[type][$rootScope.slug_pic.split("-")[2]]['img']['id'], {"landing": $stateParams.id, "slug": $rootScope.slug_pic}).then(function(data){
                    updateImg(data, obj, type);
                })
            }else{
                $http.post('/api/landing_image/', {"landing": $stateParams.id, "slug": $rootScope.slug_pic}).then(function(data){
                    updateImg(data, obj, type);
                })
            }
        };

		// настройки для slick-slider
		$scope.slickReviewsConfig = {
			arrows: false,
			dots: true,
			adaptiveHeight: true,
			method: {}
        }

	});
