angular.module('landApp').controller('IntroController',
    function ($scope, $http, $stateParams, Upload, $rootScope) {

        // настройки для Dropzone
        $scope.dzBgOptions = {
            url: '/',
            autoProcessQueue: false,
            acceptedFiles : 'image/jpeg, images/jpg, image/png',
            addRemoveLinks : false,
            uploadMultiple: false,
            thumbnailWidth: 550,
            thumbnailHeight: 300,
            maxFiles: 1,
            dictDefaultMessage:
                '<div class="dropzone__message">'+ $scope.dzmsg +'</div>' +
                '<span class="button button_base button_red dropzone__button">'+ $scope.dzbtn +'</span>',
            previewTemplate: $scope.previewTemplate
        };

        $scope.dzBgCallbacks = {
            'addedfile': function(file) {
                var allFiles = $scope.dzBgMethods.getAllFiles();
                if (allFiles.length > 1) {
                    $scope.dzBgMethods.removeFile(allFiles[0]);
                }
                $scope.file = file;
            },
            'removedfile': function() {
                $scope.file = null;
            }
        };

        $scope.dzBgMethods = {};

        var updateImg = function(data, obj){
            $rootScope.changeAndSave();
            Upload.upload({
                url: '/api/landing_image/'+data.data.id,
                data: {"img": $scope.file},
                method: 'PATCH'
            }).then(function(data){
                obj['img']['src'] = data.data.img
                obj['img']['id'] = data.data.id
                $scope.dzBgMethods.removeFile($scope.file);
            })
        }

        $scope.upload = function() {
            var obj = $scope.blocks.find(d=>d.order == $rootScope.slug_pic.split("-")[0] && d.template == $rootScope.slug_pic.split("-")[1])
            if('id' in obj.img){
                $http.patch('/api/landing_image/'+obj.img.id, {"landing": $stateParams.id, "slug": $rootScope.slug_pic}).then(function(data){
                    updateImg(data, obj);
                })
            }else{
                $http.post('/api/landing_image/', {"landing": $stateParams.id, "slug": $rootScope.slug_pic}).then(function(data){
                    updateImg(data, obj);
                })
            }
        };



        // настройки для slick-slider
        $scope.slickIntroConfig = {
            enabled: true,
            infinite: false,
            arrows: false,
            draggable: false,
            slidesToShow: 4,
            dots: true,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ],
            method: {},
            event: {
                init: function(event, slick){
                    $scope.arrowPrev = false;
                    $scope.arrowNext = true;
                    var totalCount = slick.slideCount;
                    if (totalCount <= slick.options.slidesToShow) {
                        $scope.arrowPrev = false;
                        $scope.arrowNext = false;
                    }
                },
                beforeChange: function(event, slick, currentSlide, nextSlide){
                    if ( nextSlide == slick.slideCount - slick.options.slidesToShow ) {
                        $scope.arrowNext = false;
                    } else {
                        $scope.arrowNext = true;
                    }
                    if ( nextSlide == 0 ) {
                        $scope.arrowPrev = false;
                    } else {
                        $scope.arrowPrev = true;
                    }
                }
            }
        }

    });
