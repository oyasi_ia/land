angular.module('landApp').controller("ControlPanelController",
    function ($scope, $rootScope, $timeout, addBlock) {

        function _change_ordering(order) {
            $rootScope.changeAndSave(true);
            angular.forEach($scope.blocks, function(value, key) {
                if(value.order > order){
                    value.order += 1;
                }
            });
        }

        function _refrash_tree(order) {
            $rootScope.togglePanel();
            $timeout(function() {
                var offset = angular.element('.section').eq(order).offset().top - angular.element('.top-toolbar').height();
                angular.element('body, html').animate({scrollTop: offset}, 400);
            }, 100);
        }

        $scope.addblock_customtitle = function(template, order){
            _change_ordering(order)
            $scope.blocks.push(addBlock._addblock_customtitle(template, order));
            _refrash_tree(order)
        }

        $scope.addblock_customtext = function(template, order){
            _change_ordering(order)
            $scope.blocks.push(addBlock._addblock_customtext(template, order));
            _refrash_tree(order)
        }

        $scope.addblock_customtext_center = function(template, order){
            _change_ordering(order)
            $scope.blocks.push(addBlock._addblock_customtext_center(template, order));
            _refrash_tree(order)
        }

        $scope.addblock_customtext_columns = function(template, order){
            _change_ordering(order)
            $scope.blocks.push(addBlock._addblock_customtext_columns(template, order));
            _refrash_tree(order)
        }

        $scope.addblock_custombutton = function(template, order){
            _change_ordering(order)
            $scope.blocks.push(addBlock._addblock_custombutton(template, order));
            _refrash_tree(order)
        }

        $scope.addblock_about = function(template, order){
            _change_ordering(order)
            $scope.blocks.push(addBlock._addblock_about(template, order));
            _refrash_tree(order)
        }

        $scope.addblock_action = function(template, order){
            _change_ordering(order)
            $scope.blocks.push(addBlock._addblock_action(template, order));
            _refrash_tree(order)
        }

        $scope.addblock_awards = function(template, order){
            _change_ordering(order)
            $scope.blocks.push(addBlock._addblock_awards(template, order));
            _refrash_tree(order)
        }

        $scope.addblock_intro = function(template, order){
            _change_ordering(order)
            $scope.blocks.push(addBlock._addblock_intro(template, order));
            _refrash_tree(order)
        }

        $scope.addblock_advantages = function(template, order){
            _change_ordering(order)
            $scope.blocks.push(addBlock._addblock_advantages(template, order));
            _refrash_tree(order)
        }

        $scope.addblock_gallery = function(template, order){
            _change_ordering(order)
            $scope.blocks.push(addBlock._addblock_gallery(template, order));
            _refrash_tree(order)
        }

        $scope.addblock_product = function(template, order){
            _change_ordering(order)
            $scope.blocks.push(addBlock._addblock_product(template, order));
            _refrash_tree(order)
        }

        $scope.addblock_reviews = function(template, order){
            _change_ordering(order)
            $scope.blocks.push(addBlock._addblock_reviews(template, order));
            _refrash_tree(order)
        }

        $scope.addblock_registration = function(template, order){
            _change_ordering(order)
            $scope.blocks.push(addBlock._addblock_registration(template, order));
            _refrash_tree(order)
        }

        $scope.addblock_learning = function(template, order){
            _change_ordering(order)
            $scope.blocks.push(addBlock._addblock_learning(template, order));
            _refrash_tree(order)
        }

        $scope.addblock_video = function(template, order){
            _change_ordering(order)
            $scope.blocks.push(addBlock._addblock_video(template, order));
            _refrash_tree(order)
        }

    });
