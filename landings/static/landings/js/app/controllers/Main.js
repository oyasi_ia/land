angular.module('landApp').controller('MainController',
    function ($state, $scope, $rootScope) {
        $rootScope.$state = $state;
        $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
          event.preventDefault();
          $log.info(event, toState, toParams, fromState, fromParams, error)
        });
})
