angular.module('landApp').controller('EditPanelController',
	function ($scope, $rootScope, $timeout, $templateCache, $translate, createElement) {

		$scope.editPanelState = false;

		// открываем/закрываем панель редактирования блока (на моб.)
        $scope.toggleEditPanel = function(){
            $scope.editPanelState = !$scope.editPanelState;
        };

		$scope.setSlugPic = function(order, template){
			$rootScope.slug_pic = order +'-'+ template;
		};

		$scope.saveScroll = function(clickEvent) {
			var currentOffset = angular.element(clickEvent.target).offset().top;
			$timeout(function() {
				angular.element('body, html').animate({scrollTop: angular.element(clickEvent.target).offset().top - clickEvent.clientY + 20}, 400);
			}, 100);
		};

		$scope.moveBlockUp = function(order, event) {
			$rootScope.changeAndSave(true);
			$scope.saveScroll(event);
			angular.forEach($scope.blocks, function(value, key) {
				if (value.order == order) {
					value.order -= 1;
				} else{
					if (value.order == order-1) {
						value.order += 1;
					}
				}
			});
		};

		$scope.setBlockForChange = function(block){
            $rootScope.blockForChange = $scope.blocks[$scope.blocks.indexOf(block)];
        };

		$scope.moveBlockDown = function(order, event) {
			$rootScope.changeAndSave(true);
			$scope.saveScroll(event);
			angular.forEach($scope.blocks, function(value, key) {
				if (value.order == order) {
					value.order += 1;
				} else{
					if (value.order == order+1) {
						value.order -= 1;
					}
				}
			});
		};

		$scope.addQuote = function(block){
			$rootScope.changeAndSave(true);
			var new_element = {}
			for(var key in block.quote[0]){
				new_element[key] = block.quote[0][key]
			}
			$scope.blocks[$scope.blocks.indexOf(block)].quote.push(new_element);
		}

		$scope.addPoint = function(block){
			$rootScope.changeAndSave(true);
			var new_element = {}
			for(var key in block.point[0]){
				new_element[key] = block.point[0][key]
			}
			$scope.blocks[$scope.blocks.indexOf(block)].point.push(new_element);
		}

		$scope.addElement = function(block, type, slickConfig){
			$rootScope.changeAndSave(true);
			if (type == 'slick'){
				if(block == 'gallery_1' || block == 'gallery_2'){
	                var slick_template = $rootScope.galleryLoaded
	            }else{
	                var slick_template = $rootScope.dataLoaded
	            }
	            slick_template = false
				block.elements.push(createElement[block.elements[0].template]());
				$timeout(function() {
                    slick_template = true;
                });
                $timeout(function() {
	                if (slickConfig) {
	                	slickConfig.method.slickGoTo(block.elements.length -1, true);
	                };
                }, 100);
			} else {
				block.elements.push(createElement[block.elements[0].template]());
			}
		}

		$scope.deleteBlock = function(key, order){
			$rootScope.changeAndSave(true);
			var delete_key = -1;
            angular.forEach($scope.blocks, function(value, key) {
				if(value.order == order){
					delete_key = key;
				}
				if(value.order > order){
                    value.order -= 1;
                }
            });
			if(delete_key != -1){
				$scope.blocks.splice(delete_key, 1);
			}
        }

	});
