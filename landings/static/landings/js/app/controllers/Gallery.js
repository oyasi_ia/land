angular.module('landApp').controller('GalleryController',
	function ($scope, $stateParams, Upload, $http, $rootScope, $timeout, $q) {

		$scope.files = [];
		$rootScope.galleryLoaded = true;
		$scope.settingsArray = [];

		$scope.setSlugUpload = function(order, template){
            $rootScope.slug_pic = order +'-'+ template;
        };

        $scope.setSlugImage = function(order, template, index){
            $rootScope.slug_pic = order +'-'+ template+'-'+index;
        };

        $scope.uploadPromises = [];
        $scope.postPromises = [];

		// настройки для Dropzone
		$scope.dzBgOptions = {
			url: '/',
			autoProcessQueue: false,
			acceptedFiles : 'image/jpeg, images/jpg, image/png',
			addRemoveLinks : false,
			uploadMultiple: true,
			dictDefaultMessage:
				'<div class="dropzone__message">'+$scope.dzmsg+'</div>' +
				'<span class="button button_base button_red dropzone__button">'+$scope.dzbtn+'</span>',
			previewTemplate: $scope.previewTemplate
		};

		$scope.dzBgCallbacks = {
            'addedfile': function(file) {
                $scope.files.push(file);
            }
        };

        $scope.dzBgMethods = {};

        var updateImg = function(data, obj, file){
			$rootScope.changeAndSave()
        	var slugIndex = $rootScope.slug_pic.split('-')[2];
            $scope.uploadPromises.push(
    			Upload.upload({
	                url: '/api/landing_image/' + data.data.id,
	                data: {img: file},
	                method: 'PATCH'
	            }).then(function(data){
	            	if (!slugIndex) {
		                obj.elements.push({
		                	img: {
			            		src: data.data.img,
			            		id: data.data.id
		                	}
		                });
	            	} else {
	            		obj.elements[slugIndex].img.src = data.data.img;
	            		obj.elements[slugIndex].img.id = data.data.id;
	            	}
	            })
    		);
        };

        $scope.upload = function() {

        	var slugOrder = $rootScope.slug_pic.split('-')[0];
        	var slugTemplate = $rootScope.slug_pic.split('-')[1];
        	var slugIndex = $rootScope.slug_pic.split('-')[2];
        	$rootScope.galleryLoaded = false;

        	angular.forEach($scope.files, function(file, index) {
	        	function isExist(block) {
	        		return ((block.order == slugOrder) && (block.template == slugTemplate));
	        	};
	        	var obj = $scope.blocks.find(isExist);
	        	if (slugIndex && obj.elements[slugIndex].img.id) {
	        		$scope.postPromises.push(
	        			$http.patch('/api/landing_image/' + obj.elements[slugIndex].img.id, {
	        				landing: $stateParams.id,
	        				slug: $rootScope.slug_pic
	        			}).then(function(data){
		                    updateImg(data, obj, file);
		                })
	        		);
	        	} else {
		        	$scope.postPromises.push(
			        	$http.post('/api/landing_image/', {
			        		landing: $stateParams.id,
			        		slug: $rootScope.slug_pic
			        	}).then(function(data) {
			        		updateImg(data, obj, file);
			        	})
		        	);
	        	}
        	});

        	$q.all($scope.postPromises).then(function() {
        		$q.all($scope.uploadPromises).then(function() {
		            $timeout(function() {
		                $rootScope.galleryLoaded = true;
		                $scope.postPromises = $scope.uploadPromises = $scope.files = [];
		            });
		            if ($scope.dzBgMethods.removeAllFiles) {
                		$scope.dzBgMethods.removeAllFiles();
		            };
        		})

			});
        };

		$scope.addImg = function($event){
			var obj = $scope.blocks.find(d=>d.order == $rootScope.slug_pic.split("-")[0] && d.template == $rootScope.slug_pic.split("-")[1])
			$rootScope.galleryLoaded = false;
			obj['elements'].push({'img': {'src': angular.element($event.currentTarget).find('img').attr('src')}})
			$timeout(function() {
				$rootScope.galleryLoaded = true;
			});
		}

		// настройки для slick-slider

		$scope.slickGalleryConfig = {
			arrows: false,
			variableWidth: true,
			centerMode: true,
			infinite: false,
			method: {}
        };

        $scope.buildDropzone = function(galleryItem) {
			var settings = {}
        	settings.dzOptions = {
        		url: '/',
	            autoProcessQueue: false,
	            acceptedFiles : 'image/jpeg, images/jpg, image/png',
	            addRemoveLinks : false,
	            uploadMultiple: false,
	            thumbnailWigth: null,
	            thumbnailHeight: null,
	            maxFiles: 1,
	            dictDefaultMessage:
	                '<div class="image-edit__form">'+
	                '<label class="image-edit__label">'+
	                '<span class="image-edit__button icon-camera"></span></label></div>',
	            previewTemplate: '<img hidden data-dz-thumbnail>'
	        };
	        settings.dzCallbacks = {
	        	'thumbnail': function(file, dataUrl) {
	                $scope.files.push(file);
	                $scope.upload();
	            }
	        };
	        settings.dzMethods = {};
			$scope.settingsArray.push(settings)
        };

		$scope.slickModalGalleryConfig = {
			enabled: true,
            infinite: false,
            arrows: false,
            draggable: false,
            slidesToShow: 4,
            dots: true,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ],
            method: {},
            event: {
                init: function(event, slick){
                    $scope.arrowPrev = false;
                    $scope.arrowNext = true;
                    var totalCount = slick.slideCount;
                    if (totalCount <= slick.options.slidesToShow) {
                        $scope.arrowPrev = false;
                        $scope.arrowNext = false;
                    }
                },
                beforeChange: function(event, slick, currentSlide, nextSlide){
                    if ( nextSlide == slick.slideCount - slick.options.slidesToShow ) {
                        $scope.arrowNext = false;
                    } else {
                        $scope.arrowNext = true;
                    }
                    if ( nextSlide == 0 ) {
                        $scope.arrowPrev = false;
                    } else {
                        $scope.arrowPrev = true;
                    }
                }
            }
		}

	});
