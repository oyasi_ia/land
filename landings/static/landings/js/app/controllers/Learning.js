angular.module('landApp').controller('LearningController',
	function ($scope, $http, $stateParams, Upload, $rootScope) {

		// устанавливаем элемент для дальнейшего изменения
        $scope.setPointForChange = function(block, index){
            $scope.elementForChange = $scope.blocks[$scope.blocks.indexOf(block)].point[index];
        }

        // устанавливаем элемент для дальнейшего изменения
        $scope.setQuoteForChange = function(block, index){
            $scope.elementForChange = $scope.blocks[$scope.blocks.indexOf(block)].quote[index];
        }

        $scope.setSlugPic = function(order, template, index){
            $rootScope.slug_pic = order +'-'+ template+'-'+index;
        }

	});
