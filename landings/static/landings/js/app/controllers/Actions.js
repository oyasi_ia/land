angular.module('landApp').controller('ActionsController',
	function ($scope, $timeout, $rootScope) {
		$scope.currentSlide = 0;
		$scope.slickActionsConfig = {
			arrows: false,
			method: {},
			event: {
				beforeChange: function(event, slick, currentSlide, nextSlide){
					$scope.currentSlide = nextSlide;
				}
			}
        };

		$scope.get_paragraphs = function(text){
			var paragraphs = (text ? String(text).replace(/<[^>]+>/gm, '') : '').split('\n');
			var result = []
			for(var i=0; i<paragraphs.length;i++){
				if(paragraphs[i][0] != '-'){
					result.push(paragraphs[i])
				}
			}
			return result
		}

		$scope.addToBlackList = function(action, list){
			$rootScope.changeAndSave(true);
			$rootScope.actionsLoaded = false;
			list.push(action);
			$timeout(function() {
                $rootScope.actionsLoaded = true;
            });
		}

		$scope.get_actions = function(actions, list){
			var result = []
			$scope.actions.forEach(function(value){
				if(!list.includes(value.id)){
					result.push(value)
				}
			})
			$rootScope.actionsLoaded = true;
			return result
		}
	});
