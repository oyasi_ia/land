angular.module('landApp').controller('ProductController',
    function ($scope, $http, $stateParams, Upload, $rootScope) {

        $scope.arrowPrev = false;
        $scope.arrowNext = true;

        var updateImg = function(data, obj){
            $rootScope.changeAndSave();
            Upload.upload({
                url: '/api/landing_image/'+data.data.id,
                data: {"img": $scope.file},
                method: 'PATCH'
            }).then(function(data){
                obj['img']['src'] = data.data.img
                obj['img']['id'] = data.data.id
                $scope.dzBgMethods.removeFile($scope.file);
            })
        }

        $scope.upload = function() {
            var obj = $scope.blocks.find(d=>d.order == $rootScope.slug_pic.split("-")[0] && d.template == $rootScope.slug_pic.split("-")[1])
            if('id' in obj.img){
                $http.patch('/api/landing_image/'+obj.img.id, {"landing": $stateParams.id, "slug": $rootScope.slug_pic}).then(function(data){
                    updateImg(data, obj);
                })
            }else{
                $http.post('/api/landing_image/', {"landing": $stateParams.id, "slug": $rootScope.slug_pic}).then(function(data){
                    updateImg(data, obj);
                })
            }
        };

        // настройки для Dropzone
        $scope.dzBgOptions = {
            url: '/',
            autoProcessQueue: false,
            acceptedFiles : 'image/jpeg, images/jpg, image/png',
            addRemoveLinks : false,
            uploadMultiple: false,
            thumbnailWigth: null,
            thumbnailHeight: null,
            maxFiles: 1,
            dictDefaultMessage:
                '<div class="image-edit__form">'+
                '<label class="image-edit__label">'+
                '<span class="image-edit__button icon-camera"></span></label></div>',
            previewTemplate: '<img hidden data-dz-thumbnail>'
        };

        $scope.dzBgCallbacks = {
            'thumbnail': function(file, dataUrl) {
                if($scope.file != null) {
                    $scope.dzBgMethods.removeFile($scope.file);
                };
                $scope.file = file;
                $scope.upload();
            }
        };

        $scope.setSlugPic = function(order, template){
            $rootScope.slug_pic = order +'-'+ template;
        }

        $scope.dzBgMethods = {};

    });
