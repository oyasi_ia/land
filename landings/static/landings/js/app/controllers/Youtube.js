angular.module('landApp').controller('YoutubeController',
	function ($scope, $rootScope) {

        $scope.saveModal = function(){
			if($scope.videoCode != ''){
				if($scope.videoCode.length > 20){
					var start_index = $scope.videoCode.indexOf('v=');
					var end_index = $scope.videoCode.indexOf('&');
					if(end_index == -1){
						end_index = $scope.videoCode.length;
					}
					$scope.videoCode = $scope.videoCode.substring(start_index+2, end_index);
				}
			}
			angular.forEach($scope.blocks, function(value, key){
				if(value.order == $scope.videoOrder){
					value.src = $scope.videoCode;
				}
			});
        }

	});
