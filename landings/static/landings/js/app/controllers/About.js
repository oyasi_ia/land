angular.module('landApp').controller('AboutController',
	function ($scope, $http, $sce, $timeout) {

		$scope.currentSlide = 0;
		$scope.timeline = angular.element('.timeline__area');
		$scope.totalSlides = 0;
		$scope.timelineLoaded = false;

		$http.get('/api/element_timeline').then(function(data){
            $scope.timeline_elements = data.data.results;
			$timeout(function(){
	            $scope.init()
				$scope.timelineLoaded = true;
	        });
        })

		// инициализирует тацмлайн
        $scope.init = function(){
        	$scope.timeline.pep({
				axis: 'x',
				useCSSTranslation: false,
				shouldPreventDefault: true,
				drag: function() {
					$scope.timeline.removeClass('timeline__area_transition');
				},
				rest: function(ev, obj){
					if ( parseInt( obj.$el.css('left') ) > 0 ) {
						obj.$el.css('left', 0)
					}
					else if ( parseInt( obj.$el.css('left') ) < -(obj.$el.width() - obj.$el.parent().width()) ) {
						obj.$el.css('left',  -(obj.$el.width() - obj.$el.parent().width()) )
					}
				}
			});
        };

		// настройки slick
		$scope.slickAboutConfig = {
			arrows: false,
			dots: true,
			method: {},
			event: {
				beforeChange: function(event, slick, currentSlide, nextSlide){
					$scope.moveTimeline(nextSlide);
				},
				init: function(event, slick) {
					$scope.totalSlides = slick.slideCount;
				}
			}
        };




        // сдвигает таймлайн

        $scope.moveTimeline = function(slideNum){

        	$scope.currentSlide = slideNum;
        	var timelineBlock = $scope.timeline.find('.timeline__block').eq(slideNum);

			var offsetLeft = parseInt(timelineBlock.position().left) + parseInt(timelineBlock.css('margin-left'));
			var safeMargin = 50; // безопасный отступ при выравнивании по правому краю

			// первые два слайда выравниваются по левому краю
			if ( slideNum == 0 || slideNum == 1 ) {
				$scope.timeline.css({
					left: 0
				})
			// последние два слайда выравниваются по правому краю
			} else if ( (slideNum == $scope.totalSlides - 1) || (slideNum == $scope.totalSlides - 2) ) {
				$scope.timeline.css({
					left: - offsetLeft + angular.element(window).width() - timelineBlock.width() - safeMargin
				})
			// остальные слайды выравниваются по центру страницы
			} else {
				$scope.timeline.css({
					left: - offsetLeft + angular.element(window).width()/2 - timelineBlock.width()/2
				})
			}

        };



	});
