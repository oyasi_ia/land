var app = {
		init: function() {
			app.initSliders();
			app.initMagnific();
			app.initModals();
		},
		initSliders: function() {
			if ($('.gallery__inner').length) {
				var galleryCarousel = $('.gallery__inner').slick({
					arrows: true,
					variableWidth: true,
					centerMode: true,
					infinite: false,
					swipeToSlide: true,
					prevArrow: $('.gallery-arrow-prev'),
					nextArrow: $('.gallery-arrow-next'),
				});
			};
			if ($('.carousel .actions__items').length) {
				var actionsCarousel = $('.carousel .actions__items').slick({
					prevArrow: $('.actions-arrow-prev'),
					nextArrow: $('.actions-arrow-next'),
				});
				actionsCarousel.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
					$('.actions__nav-items').find('.actions__nav-item').eq(nextSlide).addClass('actions__nav-item_active').siblings().removeClass('actions__nav-item_active');
				});
				$(document).on('click', '.actions__nav-item', function() {
					var index = $(this).index();
					$(this).addClass('actions__nav-item_active').siblings().removeClass('actions__nav-item_active');
					actionsCarousel.slick('slickGoTo', index);
				})
			};
			if ($('.carousel .reviews__inner').length) {
				$('.carousel .reviews__inner').slick({
					prevArrow: $('.reviews-arrow-prev'),
					nextArrow: $('.reviews-arrow-next'),
					dots: true,
					adaptiveHeight: true
				})
			};
			if ($('.carousel .awards__inner').length) {
				$('.carousel .awards__inner').slick({
					prevArrow: $('.awards-arrow-prev'),
					nextArrow: $('.awards-arrow-next'),
					dots: true
				})
			};
			if ($('.about-slider__items').length) {
				app.aboutSlider = $('.about-slider__items');
				app.aboutSlider.on('init', function(event, slick) {
					app.initTimeline();
					app.timelineSlides = slick.slideCount;
				});
				app.aboutSlider.slick({
					prevArrow: $('#about-arrow-prev'),
					nextArrow: $('#about-arrow-next'),
					dots: true
				});
				app.aboutSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
					app.moveTimeline(nextSlide);
				});
			};
		},
		initMagnific: function() {
			if ($('.magnific-image').length) {
				var magnific = $('.magnific-image').magnificPopup({
	                delegate: 'a',
	                type: 'image',
	                zoom: {
                        enabled: true,
                        duration: 300,
                        easing: 'ease-in-out'
                    },
	                gallery: {
                        enabled: true,
                        arrowMarkup: 
                            '<a class="magnific__arrow magnific__arrow_%dir%">' +
                                '<svg class="svg-popup-arrow-prev mfp-prevent-close" xmlns="http://www.w3.org/2000/svg" width="28.125" height="54" viewBox="0 0 28.125 54">' +
                                    '<path class="cls-1 mfp-prevent-close" d="M42.4,213.006L15.994,186.5,42.4,159.994" transform="translate(-15.5 -159.5)"/>' +
                                '</svg>' +
                                '<svg class="svg-popup-arrow-next mfp-prevent-close" xmlns="http://www.w3.org/2000/svg" width="28" height="54" viewBox="0 0 28 54">' +
                                    '<path class="cls-1 mfp-prevent-close" d="M1807.72,164.994l26.29,26.506-26.29,26.506" transform="translate(-1806.5 -164.5)"/>' +
                                '</svg>' +
                            '</a>'
                    },
	                image: {
	                    cursor: null,
	                    markup: '<div class="mfp-figure">'+
	                        '<div class="mfp-img"></div>'+
	                        '<div class="mfp-bottom-bar">'+
	                          '<div class="mfp-title"></div>'+
	                          '<div class="mfp-counter"></div>'+
	                        '</div>'+
	                      '</div>',
	                },
	                closeMarkup: '<a class="magnific__close mfp-close"><span class="icon-close"></span></a>',
	                closeBtnInside: false
	            });
			}
		},
		initTimeline: function() {
			app.timeline = $('.timeline__area');
			app.timeline.find('.timeline__block').eq(0).addClass('timeline__block_active');
			if (app.timeline.length) {
				app.timelineSlides = 0;
				app.timeline.pep({
					axis: 'x',
					useCSSTranslation: false,
					shouldPreventDefault: true,
					drag: function() {
						app.timeline.removeClass('timeline__area_transition');
					},
					rest: function(ev, obj){
						if ( parseInt( obj.$el.css('left') ) > 0 ) {
							obj.$el.css('left', 0)
						}
						else if ( parseInt( obj.$el.css('left') ) < -(obj.$el.width() - obj.$el.parent().width()) ) {
							obj.$el.css('left',  -(obj.$el.width() - obj.$el.parent().width()) )
						}
					}
				});
			};
			$(document).on('click', '.timeline__block', function() {;
				app.aboutSlider.slick('goTo', $(this).index());
			})
		},
		moveTimeline: function(slideNum){
        	app.timeline.find('.timeline__block').eq(slideNum).addClass('timeline__block_active').siblings().removeClass('timeline__block_active');
        	var timelineBlock = app.timeline.find('.timeline__block').eq(slideNum);
			var offsetLeft = parseInt(timelineBlock.position().left) + parseInt(timelineBlock.css('margin-left'));
			var safeMargin = 50; // безопасный отступ при выравнивании по правому краю
			// первые два слайда выравниваются по левому краю
			if ( slideNum == 0 || slideNum == 1 ) {
				app.timeline.css({
					left: 0
				})
			// последние два слайда выравниваются по правому краю
			} else if ( (slideNum == app.timelineSlides - 1) || (slideNum == app.timelineSlides - 2) ) {
				app.timeline.css({
					left: - offsetLeft + $(window).width() - timelineBlock.width() - safeMargin
				})
			// остальные слайды выравниваются по центру страницы
			} else {
				app.timeline.css({
					left: - offsetLeft + $(window).width()/2 - timelineBlock.width()/2
				})
			}
        },

	    getScrollbarWidth: function(){
			var module = this;
			var outer = document.createElement("div");
		    outer.style.visibility = "hidden";
		    outer.style.width = "100px";
		    outer.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps
		    document.body.appendChild(outer);
		    var widthNoScroll = outer.offsetWidth;
		    outer.style.overflow = "scroll";
		    var inner = document.createElement("div");
		    inner.style.width = "100%";
		    outer.appendChild(inner);        
		    var widthWithScroll = inner.offsetWidth;
		    outer.parentNode.removeChild(outer);
		    if ( this.hasScrollbar() ) {
		    	return widthNoScroll - widthWithScroll;
		    } else {
		    	return 0;
		    }
		},

		hasScrollbar: function(){
			 return window.innerWidth > document.documentElement.clientWidth
		},

        initModals: function() {

        	var overlay = $('.app-wrapper__overlay');

        	function openModal(modalId) {
        		overlay.addClass('app-wrapper__overlay_visible');
        		$('body').css({
					'margin-right': app.getScrollbarWidth()
				}).addClass('fixed');
        		$(modalId).addClass('modal_opened');
        	};

        	function closeModal() {
        		overlay.removeClass('app-wrapper__overlay_visible');
        		$('body').removeClass('fixed').css({
					'margin-right': 0
				});
        		$('.modal').removeClass('modal_opened');
        	};

        	function clearModal() {
        		$('.modal').find('input[type="text"]').val('');
        		$('.modal').find('textarea').val('');
        		$('.modal').find('select option:selected').prop('selected', false);
        		$('.modal').find('error').hide();
        	};

        	$(document)
	        	.on('click', '[data-modal]', function() {
	        		if ($(this).attr('data-modal') === 'open') {
	        			openModal($(this).attr('href'));
	        		} else if ($(this).attr('data-modal') === 'close') {
	        			closeModal();
	        			clearModal();
	        		};
	        	})
	        	.on('click', '.modal__container', function(event) {
	        		event.stopPropagation();
	        	});
        }
	};
	app.init();