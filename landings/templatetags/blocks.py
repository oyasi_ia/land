# coding=utf-8

import json

from django.shortcuts import render, render_to_response
from django.http import Http404, HttpResponse
from django.template import TemplateDoesNotExist, Library, Context, Template, RequestContext
from django.core.exceptions import ObjectDoesNotExist
from django.template.loader import render_to_string, get_template
from django.utils.safestring import mark_safe

from bonus.models import BonusDescription
from landings.models import ElementTimeline


register = Library()

@register.simple_tag
def render_blocks(context, e_length=None, iteration=None, is_full=None):
    html = ''
    blocks = context

    for index, block in enumerate(blocks):
        try:
            template = 'landings/blocks/{}.html'.format(block.get('template'))
        except TemplateDoesNotExist:
            return html

        content = block
        if content.get('template') in ('action_1', 'action_2'):
            content['actions'] = BonusDescription.objects.get_current()
        if content.get('template') in ('about_1', 'about_2'):
            content['timeline_elements'] = ElementTimeline.objects.all().order_by('year')
        if e_length is not None:
            content['e_length'] = e_length
        if iteration is not None:
            content['index'] = index + 1
        if is_full is not None:
            content['is_full'] = True
        if 'token' in context:
            content['token'] = context.get('token')
        html += render_to_string(template, content)
    return mark_safe(html)
