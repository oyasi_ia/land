# -*- coding: utf-8 -*-
from django.db import models
from jsonfield import JSONField
from shared.utils import upload_to
from django.contrib.auth.models import User


# Create your models here.
class Landing(models.Model):
    created_at = models.DateTimeField(verbose_name=u'Дата создания', auto_now_add=True)
    creater = models.ForeignKey(User, verbose_name=u'Автор')
    title = models.CharField(verbose_name=u'Заголовок', max_length=300, blank=True, null=True, help_text=u'Отображается только в списке лендингов')
    data = JSONField(default='[]')
    publish = models.BooleanField(u'Опубликован', default=False)
    moderating = models.BooleanField(u'Модерируется', default=False)
    domain = models.CharField(verbose_name=u'Домен', max_length=50, help_text=u'Домен')
    gtag = models.CharField(verbose_name=u'Ключ счётчика', max_length=50, default='0', help_text=u'для Google Analytics')

    objects = models.Manager()

    class Meta:
        ordering = ('created_at',)
        verbose_name = u'Лендинг'
        verbose_name_plural = u'Лендинги'

    def __unicode__(self):
        return u"{}".format(self.title)

class LandingArchive(models.Model):
    data = JSONField(default='[]')
    landing = models.ForeignKey(Landing, related_name='archive')

    objects = models.Manager()

    class Meta:
        verbose_name = u'Архив Лендингов'
        verbose_name_plural = u'Архивы Лендингов'

    def __unicode__(self):
        return u"{}".format(self.landing)

class LandingImage(models.Model):
    img = models.ImageField(upload_to=upload_to('img_landings'), null=True)
    landing = models.ForeignKey(Landing, related_name='images')
    slug = models.SlugField(max_length=50, default='')

    objects = models.Manager()

    class Meta:
        verbose_name = u'Картинка для лендинга'
        verbose_name_plural = u'Картинки для лендингов'

    def __unicode__(self):
        return u"{}".format(self.slug)

class ElementTimeline(models.Model):
    slug = models.CharField(u"Slug", max_length=255, default='', blank=True)
    title = models.CharField(u"Оглавление", max_length=255, default='', blank=True)
    text = models.CharField(u"Текст", max_length=500, default='', blank=True)
    pic = models.ImageField(u'Картинка', null=True, blank=True, upload_to=upload_to('landings_pic'))
    width = models.IntegerField(null=True, blank=True)
    month = models.CharField(u"Месяц", max_length=20, default='', blank=True)
    year = models.IntegerField(u"Год", null=True, blank=True)

    objects = models.Manager()

    class Meta():
        verbose_name = u'Элемент таймлайна'
        verbose_name_plural = u'Элементы таймлайна'


    def __unicode__(self):
        return u"{}".format(self.title)
