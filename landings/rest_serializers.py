# -*- coding: utf-8 -*-
from rest_framework import serializers
from landings.models import *


class LandingSerializer(serializers.ModelSerializer):

    class Meta:
        model = Landing
        fields = '__all__'


class LandingImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = LandingImage
        fields = '__all__'
