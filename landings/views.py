# coding=utf-8

import os

from annoying.decorators import render_to # TODO: own code?
from django.views.decorators.csrf import csrf_protect
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, JsonResponse
from django.template.loader import render_to_string
from django.template.context_processors import csrf
from django.views.decorators.http import require_POST, require_GET, require_http_methods

from landings.models import Landing, LandingArchive
from education.forms import IntroductoryLessonRegistrationForm


def __create_context(request, landing):
    current_user = {}

    if request.user.is_authenticated():
        current_user['name'] = request.user.get_full_name()
        current_user['email'] = request.user.email
        current_user['phone'] = request.user.profile.phone_mobile

    blocks = sorted(landing.data, key=lambda k: k['order'])
    context = {
        'gtag': landing.gtag,
        'data': {'token': csrf(request), 'blocks': blocks},
        'form': IntroductoryLessonRegistrationForm(initial=current_user)
    }
    if landing.creater.profile.state and landing.creater.profile.state.regional_offices:
        context['office'] = landing.creater.profile.state.regional_offices

    return context


@csrf_protect
@login_required
@require_POST
def moderating(request, pk=None):
    path = request.META.get('HTTP_REFERER','/')
    if request.user.is_superuser:
        try:
            landing = Landing.objects.get(id=pk)
        except Landing.DoesNotExist:
            return JsonResponse({'detail': 'Forbidden'}, status=403,
                                content_type='application/json')

        if not '_acceptbutton' in request.POST:
            landing.moderating = False
            landing.publish = False
            landing.save()
            return HttpResponseRedirect(path)

        landing.moderating = False
        landing.publish = True
        landing.save()

        LandingArchive.objects.create(data=landing.data, landing=landing)

        context = __create_context(request, landing)

        page = render_to_string('landings/template_landing.html', context=context)

        if not os.path.exists('generated/'+landing.domain):
            os.makedirs('generated/'+landing.domain)

        land_count = '{}'.format(LandingArchive.objects.filter(landing=landing).count())

        with open('generated/{0}/{1}.html'.format(landing.domain, landing.domain + land_count), 'wb+') as f:
            f.write(page.encode('utf-8'))

    return HttpResponseRedirect(path)


@login_required
@render_to('landings/edit_landing.html')
@require_GET
def edit_landing(request, pk=None):
    if Landing.objects.filter(id=pk, creater=request.user).exists():
        return {}
    else:
        return HttpResponseRedirect('/')


@csrf_protect
@login_required
@require_http_methods(['GET', 'POST'])
def preview_landing(request, pk=None):
    try:
        landing = Landing.objects.get(id=pk, creater=request.user)
    except Landing.DoesNotExist:
        return HttpResponseRedirect('/')

    context = __create_context(request, landing)

    if request.method == 'POST' and not landing.moderating:
        landing.moderating = True
        landing.save()
    return render(request, 'landings/view_landing.html', context)
