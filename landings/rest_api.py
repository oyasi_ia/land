from rest_framework import viewsets, permissions
from landings.rest_serializers import *
from landings.models import *


class IsOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.creater == request.user


class LandingViewSet(viewsets.ModelViewSet):
    permission_classes = [IsOwner]
    serializer_class = LandingSerializer

    def get_queryset(self):
        return Landing.objects.filter(creater=self.request.user)


class LandingImageViewSet(viewsets.ModelViewSet):
    queryset = LandingImage.objects.all()
    serializer_class = LandingImageSerializer
